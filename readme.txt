File Name: PowerPoint 83
Version: 1.0
For: TI-83+
Programmed By: Jeremy Duenas
Description:

PowerPoint 83 allows you to display your pictures on your calculator like you would using the slide show program, PowerPoint. To do this, you must have pictures stored to pic0, pic1, pic2, pic3, pic4, pic5, pic6, pic7, pic8, or pic9 (In other words, you can display up to 10 pictures, assuming that you have 10 pics stored).
F.A.Q.: 

Q. How do I store my pictures?!
A. After drawing your picture, exit to the home screen (a blank screen with a flashing black rectangle in the top left corner). Now, hit 2nd, DRAW, Left, 2. That should bring up the words "RecallPic" on the screen. Now, hit VARS, 4. Now pic which picture slot you want your picture stored to.

Q. What's the difference between auto transission and manual transission?
A. Auto Transission-   Changes between pictures every three seconds.
   Manual Transission- Changes between pictures when you hit ENTER.

Q. How can I contact you?
A. Read the next part of this readme file.


Contact Information:

Email- ToxicityJ@gmail.com
AIM- Duenos14
Yahoo- Toxicity2788

